# k3s-the-easy-way

Ce projet va me permettre de suivre les tutos visuels **"[Understanding Kubernetes in a visual way"](https://www.youtube.com/watch?v=a1Uwoq1Yv6U&list=PLmw3X80dPdlzksg6X9s23LEkLMWFGGUn5)** d'[Aurélie Vache](https://twitter.com/aurelievache).


## Pré-requis

- Avoir **Docker** installé sur sa machine (https://www.docker.com/)
- Avoir **Kubectl** installé sur sa machine (https://kubernetes.io/docs/tasks/tools/install-kubectl/)

## Créer un cluster K3s dans Docker

Nous allons utiliser K3d (une distribution de K3s qui "tourne" dans Docker): https://k3d.io/

### Installer K3d

```bash
curl -s https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash
```

### Créer le cluster

```bash
k3d cluster create panda
```

> mon cluster s'appelle **panda**

### "Récupérer" la configuration du cluster

Vous devez récupérer la configuration du cluster pour pouvoir vous connecter avec **Kubectl**. **Kubectl** utilise la variable d'environnement `KUBECONFIG` (qui va contenir la configuration). Il y a diverses façons de gérer cela (particulièrement si vous avez plusieurs clusters à gérer à partir du même poste).

Nous allons pour le moment le faire "à la main":


```bash
KUBECONFIG=$(k3d kubeconfig get panda)
```

Maintenant vous pouvez utiliser **Kubectl** pour interagir avec votre cluster, tapez la commande suivante:

```bash
kubectl get nodes
```

Vous devriez obtenir une réponse de cette forme:

```bash
NAME                 STATUS   ROLES                  AGE     VERSION
k3d-panda-server-0   Ready    control-plane,master   9m10s   v1.20.2+k3s1
```

## Understanding Kubernetes in a visual way - 01 - Pods

🖐️ Regarder la vidéo d'Aurélie : https://youtu.be/a1Uwoq1Yv6U

### Lister les pods d'un namespace

Lister les pods du namespace `kube-system`

```bash
kubectl get pods -n kube-system
```

> Obtenir la liste de tous les pods de tous les namespaces: `kubectl get pods --all-namespaces`

### Créer un pod avec le workload "busybox" dans un namespace "sandbox"

```bash
# créer le namespace sandbox
kubectl create namespace sandbox
# créer le pod
kubectl run busybox --image=busybox --restart=Never -n sandbox
```

Pour vérifier:

```bash
kubectl get pods -n sandbox
```

Vous devriez obtenir une réponse de cette forme:

```bash
NAME      READY   STATUS      RESTARTS   AGE
busybox   0/1     Completed   0          10s
```

### Editer le pod "busybox" directement dans le cluster

```bash
kubectl edit pod busybox -n sandbox
```

> tapez `:q` pour sortir du mode édition

### Supprimer le pod "busybox" 

```bash
kubectl delete pod busybox -n sandbox
```

> pour forcer la main à Kubernetes: `kubectl delete pod busybox --grace-period=0 -n sandbox`

## Understanding Kubernetes in a visual way - 02 - Deployments

🖐️ Regarder la vidéo d'Aurélie : https://youtu.be/9dyiSiyET3I

### Obtenir la liste des déploiements d'un namespace

> Ici, nous voulons avoir la liste des déploiements du namespace `kube-system`

```bash
kubectl get deployments -n kube-system
```

Vous obtiendrez une liste de cette sorte:

```bash
NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
metrics-server           1/1     1            1           6d8h
local-path-provisioner   1/1     1            1           6d8h
coredns                  1/1     1            1           6d8h
traefik                  1/1     1            1           6d8h
```

Si vous souhaitez connaître les déploiements de tous les namespaces:

```bash
kubectl get deployments --all-namespaces
```

### Afficher les informations d'un déploiement particulier

> Ici, nous voulons avoir des informations sur le déploiement `coredns` du namespace `kube-system`

```bash
kubectl get deploy coredns -o wide -n kube-system
```

Vous obtiendrez une sortie comme celle-ci:

```bash
NAME      READY   UP-TO-DATE   AVAILABLE   AGE    CONTAINERS   IMAGES                          SELECTOR
coredns   1/1     1            1           6d8h   coredns      rancher/coredns-coredns:1.8.0   k8s-app=kube-dns
```

> `-o wide` permet d'obtenir des informations complémentaires aux informations par défaut

### Créer un déploiement de manière impérative

Nous allons créer un déploiement (qui va créer un pod) à partir d'une image de **Nginx** dans le namespace `sandbox`:

```bash
kubectl create deploy nginx-deployment --image=nginx -n sandbox
# deployment.apps/nginx-deployment created
```

Et vous pouvez vérifier:

```bash
kubectl get deployments -n sandbox
```

Qui vous donnera:

```bash
NAME               READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment   1/1     1            1           85s
```

Ou bien:

```bash
kubectl get deploy nginx-deployment -o wide -n sandbox
```

Et vous obtiendrez:

```bash 
NAME               READY   UP-TO-DATE   AVAILABLE   AGE     CONTAINERS   IMAGES   SELECTOR
nginx-deployment   1/1     1            1           2m17s   nginx        nginx    app=nginx-deployment
```

### Créer un déploiement de manière déclarative

Par exemple, créons un déploiement qui va créer 3 pods à partir de l'image **busybox**. Pour cela, créez un fichier `deployment.yml`:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  selector:
    matchLabels:
      app: nginx
  replicas: 3
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
```

Puis pour déployer, tapez la commande suivante:

```bash
kubectl apply -f deployment.yml
# deployment.apps/nginx-deployment created
```

🖐️ vous remarquerez que je n'ai pas précisé le namespace, donc le déploiement a été créé dans le namespace `default`:

Donc si vous faites:

```bash
kubectl get deployments -n default
```

Vous pouvez voir que vous avez un nouveau déploiement (avec 3 pods):

```bash
NAME               READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment   3/3     3            3           2m46s
```

Et si vous utilisez la commande:

```bash
kubectl get deployments --all-namespaces
```

Vous noterez donc que nous avons déployé **Nginx** dans 2 namespaces différents:

```bash
NAMESPACE     NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   metrics-server           1/1     1            1           6d8h
kube-system   local-path-provisioner   1/1     1            1           6d8h
kube-system   coredns                  1/1     1            1           6d8h
kube-system   traefik                  1/1     1            1           6d8h
sandbox       nginx-deployment         1/1     1            1           28m
default       nginx-deployment         3/3     3            3           8m59s
```

### Modifier uniquement l'image du déploiement

Pour cela, utilisez la commande:

```bash
kubectl set image deployment/nginx-deployment nginx=nginx:1.9.1 -n sandbox
# deployment.apps/nginx-deployment image updated
```

Donc maintenant si vous faites:

```bash
kubectl get deploy nginx-deployment -o wide -n sandbox
```

Vous obtiendrez:

```bash
NAME               READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES        SELECTOR
nginx-deployment   1/1     1            1           33m   nginx        nginx:1.9.1   app=nginx-deployment
```

Et sur le namespace `default`:

```bash
kubectl get deploy nginx-deployment -o wide -n default
```

Vous pouvez noter que la version d'image n'a pas changée:

```bash
NAME               READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES   SELECTOR
nginx-deployment   3/3     3            3           15m   nginx        nginx    app=nginx
```

### Scaling: passer à 5 replicas

Augmentons le nombre de réplicas du déploiment de **Nginx** dans le namespace `sandbox`:

```bash
kubectl scale deploy nginx-deployment --replicas=5 -n sandbox
# deployment.apps/nginx-deployment scaled
```

Vous pouvez vérifier avec:

```bash
kubectl get deploy nginx-deployment -o wide -n sandbox
```

Vous pouvez noter que nous sommes bien passés à 5 réplicas

```bash
NAME               READY   UP-TO-DATE   AVAILABLE   AGE   CONTAINERS   IMAGES        SELECTOR
nginx-deployment   5/5     5            5           39m   nginx        nginx:1.9.1   app=nginx-deployment
```

### Supprimer les déploiements

Supprimons tous les déploiements de **Nginx**:

```bash
kubectl delete deploy nginx-deployment -n sandbox
# deployment.apps "nginx-deployment" deleted
kubectl delete deploy nginx-deployment -n default
# deployment.apps "nginx-deployment" deleted
```

Vérifiez avec la commande:
```bash
kubectl get deployments --all-namespaces
```

Nos déploiements ont bien été supprimés:
```bash
NAMESPACE     NAME                     READY   UP-TO-DATE   AVAILABLE   AGE
kube-system   metrics-server           1/1     1            1           6d9h
kube-system   local-path-provisioner   1/1     1            1           6d9h
kube-system   coredns                  1/1     1            1           6d9h
kube-system   traefik                  1/1     1            1           6d9h
```

## Understanding Kubernetes in a visual way - 03 - Services | Visual tutorial

🖐️ Regarder la vidéo d'Aurélie : https://www.youtube.com/watch?v=5rwYf59HWPA

Les services vont permettre d'accéder à une application à partir d'une adresse IP, aujourd'hui, voyons le service de type **ClusterIP** qui permet d'accéder à une appplication via une adresse interne du cluster

Si ce n'est pas déjà fait, relancez Docker sur votre machine. Ensuite pour avoir accés à votre cluster kube avec `kubectl`, tapez la commande suivante:

```bash
KUBECONFIG=$(k3d kubeconfig get panda)
```

Si vous ne l'avez pas déjà fait, créez un namespace `my-namespace`:

```bash
kubectl create namespace my-namespace
```

Maintenant nous allons créer un pod et son service associé à partir d'une image ubuntu (dans le namespace `my-namespace`), nous allons aussi exposer ce service sur le port `6379`:

```bash
kubectl run ubuntu-bdd --image=ubuntu --restart=Never --command sleep infinity --port=6379 --expose -n my-namespace 
```

> **Remarque**: le paramètre `--command sleep infinity` va nous permettre de "rentrer" dans notre pod en mode interactif.

Vous allez obtenir ceci en sortie:

```shell 
service/ubuntu-bdd created
pod/ubuntu-bdd created
```

Maintenant si vous tapez la commandes ci-dessous:
```bash
kubectl get services -n my-namespace
```

Et vous allez obtenir ceci:

```shell 
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)    AGE
ubuntu-bdd   ClusterIP   10.43.228.83   <none>        6379/TCP   84s
```

Donc vous voyez que vous avez crée un service de type **ClusterIP**, que ce service est disponible via l'IP `10.43.228.83` et le port disponible est `6379`.

> l'IP n'est accessible qu'à l'intérieur du cluster.

🖐️ Gardez l'IP dans un coin.

### Installons une base Redis dans notre pod

Tout d'abord "connectons" nous à notre pod en mode "`bash` interactif":

```bash
kubectl exec ubuntu-bdd -n my-namespace -it -- bash
```

Ensuite au prompt `root@ubuntu-bdd:/#` tapez les commandes suivantes pour installer une base **Redis**:

```bash
apt-get update
apt-get install redis -y 
```

Maintenant, démarrez la base:

```bash
redis-server --protected-mode no --daemonize yes
```

Vous pouvez vérifier que la base est bien lancée:

```bash
ps aux | grep redis-server
```

Maintenant ajoutons un peu de la donnée dans la base:

- Lancez le client redis: `redis-cli`
- Créez une clé-valeur: `set message "hello world"`
- Quittez le client: `exit`
- "Sortez" du pod: `exit`

### Créons un 2ème pod

Nous allons créer un 2ème pod (pas besoin d'exposer quoique ça) pour se connecter à notre base de données Redis:
```bash 
kubectl run ubuntu-cli --image=ubuntu --restart=Never --command sleep infinity -n my-namespace
```

"Entrons" dans ce nouveau pod en mode interactif:

```bash
kubectl exec ubuntu-cli -n my-namespace -it -- bash
```

Cette fois-ci aussi installons Redis pour avoir accès au client Redis:

```bash
apt-get update
apt-get install redis -y 
```

Et maintenant, essayons de nous connecter à notre serveur Redis qui "tourne" sur l'autre pod (vous vous souvenez de l'IP `10.43.228.83`):

```bash
redis-cli -h 10.43.228.83
```

Vous allez avoir le prompt suivant:
```bash
10.43.228.83:6379>
```

Et si vous tapez la commande `get message` vous obtiendrez `"hello word"`:

```shell
10.43.228.83:6379> get message
"hello world"
```

🎉 vous voyez donc l'intérêt d'un service dans kubernetes.


Si vous voulez supprimer le service et le pod: 

```bash
# supprimer le service
kubectl delete service ubuntu-bdd -n my-namespace
# supprimer le pod
kubectl delete pod ubuntu-bdd -n my-namespace
```

Suite au prochain épisode 👋

